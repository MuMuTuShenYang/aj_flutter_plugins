import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:aj_flutter_tool/aj_flutter_tool.dart';

void main() {
  const MethodChannel channel = MethodChannel('aj_flutter_tool');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await AjFlutterTool.platformVersion, '42');
  });
}
