
import 'dart:async';

import 'package:flutter/services.dart';

class AjFlutterTool {
  static const MethodChannel _channel =
      const MethodChannel('aj_flutter_tool');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  ///获取原生gradle的配置信息
  static Future<String> getMetaInfo(String metaKey) async {
    final String data = await _channel.invokeMethod('getMetaInfo', {'metaKey': metaKey});
    return data;
  }
}
