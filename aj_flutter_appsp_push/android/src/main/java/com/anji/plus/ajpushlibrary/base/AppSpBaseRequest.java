package com.anji.plus.ajpushlibrary.base;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.anji.plus.ajpushlibrary.AppSpPushConstant;
import com.anji.plus.ajpushlibrary.http.AppSpPostData;
import com.anji.plus.ajpushlibrary.util.PhoneUtil;
import com.anji.plus.ajpushlibrary.util.SPUtil;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 * Appsp
 * </p>
 */
public class AppSpBaseRequest {
    protected Context context;
    protected String appKey;

    public AppSpBaseRequest(Context context, String appKey) {
        this.context = context;
        this.appKey = appKey;
    }

    protected AppSpPostData getPostEncryptData() {
        AppSpPostData appSpPostData = new AppSpPostData();
        String regId = (String) SPUtil.get(context, AppSpPushConstant.jPushRegId, "");
        try {
            String deviceId = PhoneUtil.getDeviceId(context);
            appSpPostData.put("appKey", appKey);
            appSpPostData.put("manuToken", AppSpPushConstant.pushToken);
            appSpPostData.put("deviceType", AppSpPushConstant.brandType);
            appSpPostData.put("registrationId", regId);
            appSpPostData.put("deviceId", deviceId);
            appSpPostData.put("alias", "");
            appSpPostData.put("brand", Build.MODEL);
            appSpPostData.put("osVersion", Build.VERSION.RELEASE);
        } catch (Exception e) {

        }

        return appSpPostData;
    }
}
