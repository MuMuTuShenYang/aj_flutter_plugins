package com.anji.plus.ajpushlibrary.service;

import android.content.Context;
import android.text.TextUtils;

import com.anji.plus.ajpushlibrary.AppSpConfig;
import com.anji.plus.ajpushlibrary.AppSpLog;
import com.anji.plus.ajpushlibrary.base.AppSpBaseRequest;
import com.anji.plus.ajpushlibrary.http.AppSpCallBack;
import com.anji.plus.ajpushlibrary.http.AppSpHttpClient;
import com.anji.plus.ajpushlibrary.http.AppSpPostData;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 * 将厂商token和极光的regId上传给服务器
 * </p>
 */
public class AppSpServiceImpl extends AppSpBaseRequest implements IAppSpService {
    private AppSpHandler appSpHandler;

    public AppSpServiceImpl(Context mContext, String appKey, AppSpHandler appSpHandler) {
        super(mContext, appKey);
        this.appSpHandler = appSpHandler;
    }

    @Override
    public void putPushInfo() {
        if (TextUtils.isEmpty(appKey)) {
            AppSpLog.e("putPushInfo Appkey is null or empty");
            return;
        }
        AppSpPostData appSpPostData = getPostEncryptData();
        AppSpHttpClient client = new AppSpHttpClient();
//        client.request(AppSpRequestUrl.Host + AppSpRequestUrl.putPushInfo, appSpPostData, new AppSpCallBack() {
        client.request(AppSpConfig.requestUrl, appSpPostData, new AppSpCallBack() {
            @Override
            public void onSuccess(String data) {
                if (appSpHandler != null) {
                    appSpHandler.handleSuccess(data);
                }
            }

            @Override
            public void onError(String code, String msg) {
                if (appSpHandler != null) {
                    appSpHandler.handleExcption(code, msg);
                }
            }
        });

    }

}
