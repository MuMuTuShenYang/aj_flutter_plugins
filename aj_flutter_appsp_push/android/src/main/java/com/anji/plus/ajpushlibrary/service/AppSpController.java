package com.anji.plus.ajpushlibrary.service;

import android.content.Context;

import com.anji.plus.ajpushlibrary.base.AppSpBaseController;


/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 *
 * </p>
 */
public class AppSpController extends AppSpBaseController {
    public AppSpController(Context mContext, String appKey) {
        super(mContext, appKey);
    }

    /**
     * @param iAppSpCallback 结果回调接口
     */
    public void putPushInfo(IAppSpCallback iAppSpCallback) {
        AppSpHandler appSpHandler = new AppSpHandler();
        appSpHandler.setAppSpCallback(iAppSpCallback);
        IAppSpService appspVersionService = new AppSpServiceImpl(getContext(), getAppKey(), appSpHandler);
        appspVersionService.putPushInfo();
    }
}
